import unittest
import requests
import json
from main import userapi

IP = "0.0.0.0"
PORT = "15555"
URL = "http://" + IP + ":" + PORT
HEADERS = {"Content-Type":"application/json"}
API_KEY = ""

class BasicTests(unittest.TestCase):

	def test_hello(self):
		response = requests.get(URL + "/" + API_KEY)
		self.assertEqual(200, response.status_code)

		obj = json.loads(response.content.decode('utf-8'))
		self.assertIn('msg', obj)

if __name__ == '__main__':
	unittest.main()
