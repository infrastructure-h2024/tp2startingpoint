from flask import *
from main import userdomain as dom
from main import myoswrap
from main.customExceptions import *
import sys
import json

app = Flask('userapi')

@app.route('/')
def route_hello():
	return jsonify({'msg':dom.hello()}), 200

@app.route('/route')
def route_getUsers():
	return jsonify({'msg':'Not implemented'}), 200

@app.route('/route1')
def route_addUser():
	return jsonify({'msg':'Not implemented'}), 200


@app.route('/route2')
def route_resetUsers():
	return jsonify({'msg':'Not implemented'}), 200


@app.route('/route3')
def route_delUser(username):
	return jsonify({'msg':'Not implemented'}), 200

@app.route('/route4')
def route_getLogs():
	return jsonify({'msg':'Not implemented'}), 200

if __name__ == "__main__":
	if len(sys.argv) >= 2:
		try:
			app_port = int(sys.argv[1])
		except ValueError as ve:
			app_port = 5558

		app.run(debug=True, host='0.0.0.0', port=app_port)
	else:
		raise ValueError('No starting port for the application')

